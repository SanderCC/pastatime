import "./css/App.css";
import { BrowserRouter as Router } from "react-router-dom";
import { Basket } from "./hooks/useBasket";
import MainRouter from "./components/navigation/routers/MainRouter";
import { MyTheme } from "./hooks/useMyTheme";
import Navbar from "./components/navigation/Navbar";
import Footer from "./components/particles/Footer";

function App() {
  return (
    <MyTheme>
      <div style={{ background: "default" }}>
        <Basket>
          <Router>
            <Navbar />
            <MainRouter />
          </Router>
        </Basket>
        <Footer />
      </div>
    </MyTheme>
  );
}

export default App;
