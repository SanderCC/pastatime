export function numberToEuro(value:number) : string {
    let number = new Intl.NumberFormat().format(value).replaceAll(",",".")

    if(number.includes('.')) {
        if(number.split('.')[1].length < 2) {
            number = number + "0"
        }
    } else {
        number = number + ".00"
    }

    return `€ ${number}`
}