export function arraySum(array:number[]) : number {
    if(array.length === 0) return 0

    let sum = 0

    for(let i = 0; i < array.length; i++) {
        sum += array[i]
    }

    return sum
}