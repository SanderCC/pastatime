import {Item} from "../domain/Item";

export default function getMenuItems() : Item[] {
    return menuItems
}

const menuItems : Item[] = [
    {
        name: "Kleine Pasta Bolognaise",
        picture: "https://i.imgur.com/H4XuqPB.png",
        details: "Tomatensaus met runds en varkensgehakt.",
        price: 6.80,
        comments: ""
    },
    {
        name: "Grote Pasta Bolognaise",
        picture: "https://i.imgur.com/H4XuqPB.png",
        details: "Tomatensaus met runds en varkensgehakt.",
        price: 8.30,
        comments: ""
    },
    {
        name: "Kleine Pasta Carbonara",
        picture: "https://i.imgur.com/H4XuqPB.png",
        details: "Roomsaus met ham.",
        price: 6.80,
        comments: ""
    },
    {
        name: "Grote Pasta Carbonara",
        picture: "https://i.imgur.com/H4XuqPB.png",
        details: "Roomsaus met ham.",
        price: 8.30,
        comments: ""
    },
    {
        name: "Kleine Pasta Curry",
        picture: "https://i.imgur.com/H4XuqPB.png",
        details: "Met kip en licht pikant.",
        price: 6.80,
        comments: ""
    },
    {
        name: "Grote Pasta Curry",
        picture: "https://i.imgur.com/H4XuqPB.png",
        details: "Met kip en licht pikant.",
        price: 8.30,
        comments: ""
    },
    {
        name: "Kleine Pasta Napolitana",
        picture: "https://i.imgur.com/H4XuqPB.png",
        details: "Vegetarisch",
        price: 6.80,
        comments: ""
    },
    {
        name: "Grote Pasta Napolitana",
        picture: "https://i.imgur.com/H4XuqPB.png",
        details: "Vegetarisch",
        price: 8.30,
        comments: ""
    },
    {
        name: "Kleine Pasta Broccoli Roomsaus",
        picture: "https://i.imgur.com/H4XuqPB.png",
        details: "Vegetarisch",
        price: 6.80,
        comments: ""
    },
    {
        name: "Grote Pasta Broccoli Roomsaus",
        picture: "https://i.imgur.com/H4XuqPB.png",
        details: "Vegetarisch",
        price: 8.30,
        comments: ""
    }
]

