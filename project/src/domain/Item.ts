export interface Item {
  name: string;
  picture: string;
  details: string;
  price: number;
  comments?: string;
}

export function getDefaultBasketItem() : Item {
  return {
    name: "Banana",
    picture: "https://i.imgur.com/H4XuqPB.png",
    details: "Yellow banana imported from Spain",
    price: 2
  }
}

export function itemToString(item:Item) : string {
  return `${item.name} (€${item.price})`
}