import {createContext, useContext, useState} from "react";
import {Item} from "../domain/Item";

const defItem = {basket: [], add: (item:Item) => {console.log("add "+item.name)}, remove: (item:Item) => { console.log("remove "+item.name) }}
export var BasketContext = createContext<{basket: Item[], add: (item:Item) => void, remove: (item:Item) => void}>(defItem)

export function useBasket() {
  return useContext(BasketContext)
}

export function Basket({children}: {children:JSX.Element}) {
  const [basket, setBasket] = useState<{items: Item[]}>({items: []})

  function addItem(item:Item) : void {
    let tempBasket = basket.items
    tempBasket.push(item)
    console.log("add item: "+item.name)
    setBasket({items: tempBasket})
  }


  function removeItem(item:Item) : void {
    let tempBasket = basket.items
    const itemIndex = tempBasket.indexOf(item)
    if(itemIndex >= 0) {
      console.log("remove item: "+itemIndex)
      tempBasket.splice(itemIndex, 1)
      setBasket({items: tempBasket})
    } else {
      console.error("ITEM NOT FOUND")
    }
  }

  return (
    <BasketContext.Provider value={{basket: basket.items, add: addItem, remove: removeItem}}>
      {children}
    </BasketContext.Provider>
  )
}