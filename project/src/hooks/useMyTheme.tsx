import { PaletteMode } from "@mui/material";
import {createContext, useContext, useMemo, useState} from "react";
import { ThemeProvider, createTheme } from '@mui/material/styles'

export const getDesignTokens = (mode : PaletteMode) => ({
  palette: {
    type: 'light',
    primary: {
      main: '#26a429',
    },
    secondary: {
      main: '#ff9400',
    },
    background: {
      default: '#e8e1d9',
      paper: '#f6f6f6',
    },
    text: {
      primary: '#25391c',
      secondary: '#435c30',
      disabled: '#24941f',
    },
  },
});

export function useMyTheme() {
  return useContext(ColorModeContext)
}

export const ColorModeContext = createContext({ toggle: () => { }, mode: "light" });

export function MyTheme({children} : {children : JSX.Element}) {
  const [mode, setMode] = useState<PaletteMode>('light');
  const colorMode = useMemo(
    () => ({
      toggle: () => {
        setMode((prevMode) => (prevMode === 'light' ? 'dark' : 'light'));
      },
      mode: mode
    }),
    [mode],
  );

  const theme = useMemo(
    () =>
      createTheme(getDesignTokens(mode)), [mode],
  );

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        {children}
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
}
