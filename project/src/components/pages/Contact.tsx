import Container from "@mui/material/Container";
import {Typography} from "@mui/material";
import Map from "../particles/Map";

export default function Contact() {
    return (<>
            <Map />
            <Container maxWidth="md">
                <div className={"flexCards centerCards"}>
                    <div style={{margin: 30}}>
                        <Typography variant={"h5"} sx={{color: "secondary.main"}}>
                            Adres
                        </Typography>
                        <Typography variant={"body2"} sx={{color: "#e8e8e8"}}>
                            Sint-Katelijnevest 70,
                            <br />
                            2000 Antwerpen
                        </Typography>

                        <Typography variant={"h5"} sx={{color: "secondary.main", mt: 2}}>
                            Contact
                        </Typography>
                        <Typography variant={"body2"} sx={{color: "#e8e8e8"}}>
                            business@sanderc.net
                        </Typography>
                        <Typography variant={"body2"} sx={{color: "#e8e8e8"}}>
                            +324 OO II OO II
                        </Typography>
                    </div>
                    <div style={{margin: 30}}>
                        <Typography variant={"h5"} sx={{color: "secondary.main"}}>
                            Openingsuren
                        </Typography>
                        <Typography variant={"body2"} sx={{color: "#e8e8e8"}}>
                            Maandag	11:30–19:00<br/>
                            Dinsdag	11:30–19:00<br/>
                            Woensdag 11:30–14:00, 17:00–18:30<br/>
                            Donderdag 11:30–19:00<br/>
                            Vrijdag 11:30–18:30<br/>
                            Zaterdag 12:00–18:30<br/>
                            Zondag gesloten
                        </Typography>
                    </div>
                </div>
            </Container>
        </>
    )
}