import HeroSection from "../particles/Home/HeroSection";
import About from "../particles/Home/About";

export default function Home() {
  return (
    <>
      <HeroSection />
      <About />
    </>
  );
}
