import MenuItems from "../particles/Menu/MenuItems";
import Basket from "../particles/Menu/Basket";
import {Box} from "@mui/material";

export default function Menu() {
    return <Box className={"flexCards centerCards"}>
        <Box>
            <MenuItems />
        </Box>
        <Box>
            <Basket />
        </Box>
    </Box>
}

