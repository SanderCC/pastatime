import {useBasket} from "../../hooks/useBasket";
import {Typography} from "@mui/material";
import {itemToString} from "../../domain/Item";
import Map from "../particles/Map";

export default function Confirmation() {
    const basketManager = useBasket()
    return (<>
        <div className={"flexCards centerCards"}>
            <div style={{margin: 10, marginRight: 40, color: "#eaeaea"}}>
                <Typography color={"secondary.main"} variant={"h3"}>We hebben je bestelling ontvangen!</Typography>
                <Typography sx={{m: 3}} variant={"body2"}>Jouw bestelling bevat:</Typography>
                <ul>
                    {basketManager.basket.map(i => (
                        <li key={Math.random()}>{itemToString(i)}</li>
                    ))}
                </ul>
                <Typography variant={"body1"}>Je mag je bestelling komen ophalen in ons restaurant en daar de betaling afronden!</Typography>
            </div>
            <img alt={"qr"}
                src={"https://i.imgur.com/o64hs8u.jpg"}/>
        </div>
        <Map/>
    </>)
}