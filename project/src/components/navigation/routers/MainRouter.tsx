import {Route, Switch} from "react-router";
import Menu from "../../pages/Menu";
import Home from "../../pages/Home";
import Contact from "../../pages/Contact";
import Confirmation from "../../pages/Confirmation";

export default function MainRouter() {
    return (
        <Switch>
            <Route path="/Menu" component={Menu}/>
            <Route path="/Contact" component={Contact}/>
            <Route path="/Confirmation" component={Confirmation}/>
            <Route path="/" component={Home}/>
        </Switch>
    );
}
