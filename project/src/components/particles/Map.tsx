export default function Map() {
    return <div className={"flexCards centerCards"} style={{padding: 50}}>
        <iframe title="gmaps" width="800" height="300" id="gmap_canvas"
                src="https://maps.google.com/maps?q=Pasta%20Time&t=&z=15&ie=UTF8&iwloc=&output=embed"
                frameBorder="0" scrolling="no" style={{margin: 0, maxWidth: "100%", borderRadius: 10}}/>
    </div>
}