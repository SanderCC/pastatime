import {Item} from "../../../domain/Item";
import {useBasket} from "../../../hooks/useBasket";
import {IconButton, Typography} from "@mui/material";
import {numberToEuro} from "../../../functions/currency";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";

export function BasketItem({item}: { item: Item }) {
    const basketManager = useBasket()
    return <>
        <div className={"flexCards"}>
            <Typography component="div" variant="h6">{`${item.name} (${numberToEuro(item.price)})`}</Typography>
            <IconButton sx={{color: "red",m: "auto"}} onClick={() => basketManager.remove(item)}>
                <CloseRoundedIcon />
            </IconButton>
        </div>

    </>
}