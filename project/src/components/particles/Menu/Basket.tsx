import {useBasket} from "../../../hooks/useBasket";
import {Paper, Typography} from "@mui/material";
import {numberToEuro} from "../../../functions/currency";
import {arraySum} from "../../../functions/array";
import {BasketItem} from "./BasketItem";
import {useHistory} from "react-router-dom";
import Button from "@mui/material/Button";

export default function Basket() {
    const history = useHistory()
    const basketManager = useBasket()
    let keyIndex = 1
    return <>
        <Typography sx={{color: "secondary.main", m: 2, ml: 5, mt: 5, minWidth: 350}} variant={"h4"}>
            Geselecteerd <small>{basketManager.basket.length > 0 ? `(${numberToEuro(arraySum(basketManager.basket.map(i => i.price)))})` : "" }</small>
        </Typography>
        <Paper sx={{m:1}}>
            {basketManager.basket.length > 0 ? <>{basketManager.basket.map(i => (
                    <div key={keyIndex++} style={{padding:5, paddingLeft: 10, borderRadius: 5}} className={"darkenEven"}>
                        <BasketItem item={i}/>
                    </div>
            ))}
                <div className={"flexCards centerCards"}>
                    <Button sx={{m:2}} variant={"contained"} color={"secondary"} onClick={() => history.push("/Confirmation")}>
                        Bevestig bestelling
                    </Button>
                </div>
            </> : <div className={"flexCards centerCards"}><img alt={"empty basket"} style={{width: "70%", margin: "20px auto"}} src={"https://i.imgur.com/fqQZ885.png"} /></div>}
        </Paper>
    </>
}