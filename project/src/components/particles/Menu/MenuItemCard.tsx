import {Item} from "../../../domain/Item";
import {Box, Card, CardContent, IconButton, Typography} from "@mui/material";
import {useState} from "react";
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import {numberToEuro} from "../../../functions/currency";
import {useBasket} from "../../../hooks/useBasket";

export default function MenuItemCard({item}: { item: Item }) {
    const [hover, setHover] = useState(false)
    const basketManager = useBasket()
    return (
        <Card sx={{display: 'flex', minWidth: 500, width: "fit-content", m: 1}} onMouseEnter={() => setHover(true)} onMouseLeave={() => setHover(false)}>
                <CardContent sx={{flex: '1 0 auto'}}>
                    <Box className={"flexCards"}>
                        <div style={{margin: "auto 1px", width: 450}}>
                            <Typography component="div" variant="h5">
                                {item.name}
                            </Typography>
                            <Typography variant="subtitle2" color="text.secondary" component="div" sx={{width: 300}}>
                                {item.details}
                            </Typography>
                        </div>
                        <Box sx={{height: 60, ml: 1, m: 2, textAlign: "center"}}>
                            <Typography sx={{pl: 3, pb: 1, fontSize: 20}}>
                                {numberToEuro(item.price)}
                            </Typography>
                            <IconButton onClick={() => basketManager.add(item)}>
                                <AddShoppingCartIcon sx={hover ? {fontSize: 25, color: "primary.main"} : {fontSize: 25}} />
                            </IconButton>
                        </Box>
                    </Box>
                </CardContent>
        </Card>)
}