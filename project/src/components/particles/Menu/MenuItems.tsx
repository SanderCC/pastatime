import {Typography} from "@mui/material";
import getMenuItems from "../../../functions/getMenuItems";
import MenuItemCard from "./MenuItemCard";

export default function MenuItems() {
    return <>
        <Typography sx={{color: "secondary.main", m: 2, ml: 5, mt: 5}} variant={"h4"}>Menu</Typography>
        {getMenuItems().map(i => {
            return <MenuItemCard key={i.name} item={i} />
        })}
    </>
}