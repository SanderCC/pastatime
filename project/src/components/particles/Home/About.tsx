import { Typography } from "@mui/material";

const aboutItems = [
  {
    imageLeft: true,
    header: "Heerlijke bouillon, een puur smakelijk genot",
    title: "Pasta Time. Een ware evolutie.",
    text: "Doorheen de jaren is Pasta Time sterk geëvolueerd. We proberen te streven naar versheid, en proberen de beste smaken te combineren met een klantvriendelijke service. Onze authentieke en duurzame wijze van het creëren van de beste pasta wacht op jou!",
    image: "./pasta-2.jpg",
  },
  {
    imageLeft: false,
    header: "Baad je in de wondere wereld van pasta",
    title: "Pasta Time. Een ware evolutie",
    text: "Pasta Time biedt verschillende soorten sauzen aan. Voor ieder wat wils!  We gebruiken one expertise om kwalitatieve pasta aan te bieden. Eveneens kan je ter plekke genieten van je lekkere maaltijd.",
    image: "./pasta.jpg",
  },
];

export default function About() {
  return (
    <>
      {aboutItems.map((i) => (
        <AboutCard
          imageLeft={i.imageLeft}
          header={i.header}
          title={i.title}
          text={i.text}
          image={i.image}
        />
      ))}
    </>
  );
}

function AboutCard({
  imageLeft,
  header,
  title,
  text,
  image,
}: {
  imageLeft: boolean;
  header: string;
  title: string;
  text: string;
  image: string;
}) {
  return (
    <div style={{ margin: 10 }}>
      <Typography
        style={{
          fontFamily: "Montserrat",
          fontSize: 20,
          margin: 20,
          textAlign: "center",
          color: "white",
          fontWeight: 900,
        }}
      >
        {header}
      </Typography>
      <div className={"flexCards centerCards"}>
        {imageLeft ? (
          <AboutCardLeft title={title} text={text} image={image} />
        ) : (
          <AboutCardRight title={title} text={text} image={image} />
        )}
      </div>
    </div>
  );
}

function AboutCardLeft({
  title,
  text,
  image,
}: {
  title: string;
  text: string;
  image: string;
}) {
  return (
    <>
      <div>
        <img
          src={image}
          alt="cup"
          style={{ width: 600, height: 250, marginTop: 20, borderRadius: 5 }}
        />
      </div>
      <div style={{ width: "50%", padding: 30 }}>
        <AboutText title={title} text={text} />
      </div>
    </>
  );
}

function AboutCardRight({
  title,
  text,
  image,
}: {
  title: string;
  text: string;
  image: string;
}) {
  return (
    <>
      <div style={{ width: "50%", padding: 30 }}>
        <AboutText title={title} text={text} />
      </div>
      <div>
        <img
          src={image}
          alt="cup"
          style={{ width: 600, height: 250, marginTop: 20, borderRadius: 5 }}
        />
      </div>
    </>
  );
}

function AboutText({ title, text }: { title: string; text: string }) {
  return (
    <>
      <Typography
        style={{
          color: "white",
          fontFamily: "Montserrat",
          fontWeight: "bold",
          fontSize: 25,
          marginBottom: 5,
          marginLeft: 20,
        }}
      >
        {title}
      </Typography>
      <Typography
        style={{
          color: "white",
          fontFamily: "Montserrat",
          fontWeight: "bold",
          marginLeft: 20,
        }}
      >
        {text}
      </Typography>
    </>
  );
}
