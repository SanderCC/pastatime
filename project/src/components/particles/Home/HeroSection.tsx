import { Button, Grid, Typography } from "@mui/material";
import { useHistory } from "react-router-dom";
import "../../../css/HeroSection.css";
export default function HeroSection() {
  let history = useHistory();
  return (
    <>
      <Grid
        container
        className="background-image"
        justifyContent="left"
        display="flex"
        alignItems="left"
        sx={{ height: "90%" }}
      >
        <Grid item>
          <div style={{ height: "90vh" }}>
            <Typography
              style={{
                fontFamily: "Montserrat",
                fontSize: 50,
                textAlign: "left",
                maxWidth: "750px",
                marginLeft: "4vw",
                color: "white",
                fontWeight: 900,
                paddingTop: 70,
              }}
            >
              Verse pasta. Bestel nu.
            </Typography>
            <Typography
              style={{
                fontFamily: "Montserrat",
                fontSize: 20,
                textAlign: "left",
                maxWidth: "750px",
                marginLeft: "4vw",
                color: "white",
                fontWeight: 900,
              }}
            >
              De beste pasta van Antwerpen!
            </Typography>
            <Button
              variant="contained"
              style={{
                marginTop: 20,
                textAlign: "left",
                marginLeft: "4vw",
                backgroundColor: "#ea993d",
                textTransform: "none",

                height: 45,
              }}
              onClick={() => history.push(`/Menu`)}
            >
              <Typography
                style={{
                  fontFamily: "Montserrat",
                  fontWeight: "bold",
                  paddingLeft: 7,
                  paddingRight: 7,
                }}
              >
                Bestel!
              </Typography>
            </Button>
          </div>
        </Grid>
      </Grid>
    </>
  );
}
